Fix for Content editor crashes IE 11 2529 in RefineryCMS
=========
http://github.com/refinery/refinerycms/issues/2529 

A temporary fix for the issue of IE 11 crashing when running the content editor as proposed by digisquid.

Installation
--------------

```sh
git clone git@bitbucket.org:Rodger_FridayMedia/ie11-refinery-wysiwyg-fix.git ie11-fix
cd ie11-fix
cp code_to_remove.js ie11-fix.rb [your-refinery-project-root-directory]
cd [your-refinery-project-root-directory]
ruby ie11-fix.rb
rm ie11-fix.rb code_to_remove.js
```

Notes
-------

If you have already overriden refinery/boot_wym for whatever reason, comment out lines 10, 12 and 21 of ie11-fix.rb.